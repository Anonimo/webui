# Anonimo WebUI

A web client for the Anonimo platform

## Getting Started

These instructions will help you set up the webui on your local development machine.

### Prerequisites

Please install and configure the following dependencies in the listed order in order to continue installing the Anonimo webui.


1. [node](https://nodejs.org/en/)


### Installing

Follow these instructions to install a copy of the Anonimo webui on your local machine.

Clone the webui repository to it's project path.

```
git clone https://gitlab.com/Anonimo/webui $GOPATH/src/anonimo/webui && cd $GOPATH/src/anonimo/webui
```

Install the webui dependencies.

```
cd webui && npm install && cd ..
```

Create a `env.json` file inside of your `/src` directory with the following contents.

```
{
  "backend": "localhost:4000"
}
```

You could also run the following command.

```
echo "{\"backend\", \"localhost:4000\"}" > src/env.json
```

## Running the project

Assuming that the server is already running and you have already configured your webui to communicate with it, run the following command.

```
cd webui && npm start
```

## Contributing

Make sure to write code according to the following guidelines.

1. [StandardJS](https://standardjs.com/rules.html)
